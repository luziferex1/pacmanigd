﻿using UnityEngine;
using System.Collections;

public class AIGhostman : MonoBehaviour {

	// Use this for initialization

	public float speed = 0.4f;
	private Vector2 destination;
	private Vector2 direction;
	private Vector2 nextDirection;

	void Awake() {
		destination = Vector2.zero;
		direction = Vector2.zero;
		nextDirection = Vector2.zero;
	}

	// Use this for initialization
	void Start () {
		direction = Vector2.right; // or direction = new Vector2(1, 0);
		destination = transform.position;
	}

	// Update is called once per frame
	void FixedUpdate () {
		// move closer to destination
		Vector2 p = Vector2.MoveTowards(transform.position, destination, speed);
		GetComponent<Rigidbody2D>().MovePosition(p);



		// if pacman is in the center of a tile
		if (Vector2.Distance(destination, transform.position) < 0.00001f) {
			if (Valid(Vector2.right)) {
				direction = Vector2.right;
			}
			else if (Valid(Vector2.left)) {
				direction = Vector2.left;
			}
			else if (Valid(Vector2.up)) {
				direction = Vector2.up;
			}
			else if (Valid(Vector2.down)) {
				direction = Vector2.down;
			}
			else {
				direction = direction;
				if (Valid(direction)) {
					destination = (Vector2)transform.position + direction;
				}
			}
			destination = (Vector2)transform.position + direction;
		}
	}

	bool Valid(Vector2 direction) {
		Vector2 pos = transform.position;
		direction += new Vector2(direction.x * 0.45f, direction.y * 0.45f);
		RaycastHit2D hit = Physics2D.Linecast(pos + direction, pos);
		if (hit.collider == null) {
			// Hit nothing
			return true;
		}
		else {
			// Hit something
			Debug.Log("LineCast hit: " + hit.transform.gameObject.name);
			if (hit.transform == this.transform) {
				return true;
			}
			else if (hit.transform.name == "pacdot") {
				return true;
			}
			else {
				return false;
			}
		}
	}
}
